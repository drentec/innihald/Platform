<?php

namespace App\Tests;

use App\DataFixtures\DocumentFixtures;
use Doctrine\Common\Annotations\Annotation\IgnoreAnnotation;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApplicationAvailabilityFunctionalTest
 * @package App\Tests
 *
 * @IgnoreAnnotation("dataProvider")
 */
class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    use FixturesTrait;

    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $this->loadFixtures([DocumentFixtures::class]);

        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertResponseIsSuccessful();
    }

    public function urlProvider()
    {
        yield ['/'];
        yield ['/document/'];
        yield ['/document/1'];
        yield ['/document/1/edit'];
        yield ['/document/new'];
    }
}
