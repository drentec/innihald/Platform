<?php

namespace App\Entity;

use App\Entity\Traits\SoftDeletable;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentContentRepository")
 */
class DocumentContent
{
    use SoftDeletable;
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="text")
     */
    private string $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $extractionMethod;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", inversedBy="content", cascade={"persist", "remove"})
     */
    private Document $document;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getExtractionMethod(): ?string
    {
        return $this->extractionMethod;
    }

    public function setExtractionMethod(string $extractionMethod): self
    {
        $this->extractionMethod = $extractionMethod;

        return $this;
    }

    public function getDocument(): ?Document
    {
        return $this->document;
    }

    public function setDocument(?Document $document): self
    {
        $this->document = $document;

        return $this;
    }
}
