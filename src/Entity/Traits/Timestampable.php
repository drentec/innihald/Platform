<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * BaseEntity.php of project Innihald.
 * Created by user marian at 24.01.20.
 */

namespace App\Entity\Traits;

use DateTime;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations
use Doctrine\ORM\Mapping as ORM;

trait Timestampable
{

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private DateTime $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private DateTime $updated;

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }
}