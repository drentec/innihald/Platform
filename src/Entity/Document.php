<?php

namespace App\Entity;

use App\Entity\Traits\SoftDeletable;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 */
class Document
{
    use SoftDeletable;
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?String $title = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?String $description = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PhysicalFile", inversedBy="documents")
     */
    private ?PhysicalFile $physicalFile = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\DocumentContent", mappedBy="document", cascade={"persist", "remove"})
     */
    private $content;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhysicalFile(): ?PhysicalFile
    {
        return $this->physicalFile;
    }

    public function setPhysicalFile(?PhysicalFile $physicalFile): self
    {
        $this->physicalFile = $physicalFile;

        return $this;
    }

    public function getContent(): ?DocumentContent
    {
        return $this->content;
    }

    public function setContent(?DocumentContent $content): self
    {
        $this->content = $content;

        // set (or unset) the owning side of the relation if necessary
        $newDocument = null === $content ? null : $this;
        if ($content->getDocument() !== $newDocument) {
            $content->setDocument($newDocument);
        }

        return $this;
    }
}
