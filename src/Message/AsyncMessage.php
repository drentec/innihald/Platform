<?php
/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * AsyncMessage.php of project Innihald.
 * Created by user marian at 27.01.20.
 */

namespace App\Message;


interface AsyncMessage
{
    public function getName(): string;
}