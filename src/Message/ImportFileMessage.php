<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * ImportFileMessage.php of project Innihald.
 * Created by user marian at 26.01.20.
 */

namespace App\Message;


class ImportFileMessage implements AsyncMessage
{
    private string $filename;

    private string $extension;

    /**
     * ImportFileMessage constructor.
     * @param string $filename
     * @param string $extension
     */
    public function __construct(string $filename, string $extension)
    {
        $this->filename = $filename;
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    public function getName(): string
    {
        return "import_file";
    }
}