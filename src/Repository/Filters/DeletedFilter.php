<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * DeletedFilter.php of project Innihald.
 * Created by user marian at 24.01.20.
 */

namespace App\Repository\Filters;


use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class DeletedFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if ($targetEntity->hasField("deletedAt")) {
            $date = date("Y-m-d h:m:s");

            return $targetTableAlias.".deleted_at < '".$date."' OR ".$targetTableAlias.".deleted_at IS NULL";
        }

        return "";
    }
}