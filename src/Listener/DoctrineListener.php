<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * DoctrineListener.php of project Innihald.
 * Created by user marian at 24.01.20.
 */

namespace App\Listener;


use Doctrine\ORM\Event\PreFlushEventArgs;

class DoctrineListener
{
    public function preFlush(PreFlushEventArgs $event) {
        $em = $event->getEntityManager();

        foreach ($em->getUnitOfWork()->getScheduledEntityDeletions() as $object) {
            if (method_exists($object, "getDeletedAt")) {
                if ($object->getDeletedAt() instanceof \Datetime) {
                    continue;
                } else {
                    $object->setDeletedAt(new \DateTime());
                    $em->merge($object);

                    $em->persist($object);
                }
            }
        }
    }
}