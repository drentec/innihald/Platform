<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * PhysicalFileService.php of project Innihald.
 * Created by user marian at 24.01.20.
 */

namespace App\Service\Entity;

use App\Entity\PhysicalFile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PhysicalFileService
{
    private ValidatorInterface $validator;

    private EntityManagerInterface $em;

    /**
     * PhysicalFileService constructor.
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $em
     */
    public function __construct(ValidatorInterface $validator, EntityManagerInterface $em)
    {
        $this->validator = $validator;
        $this->em = $em;
    }

    public function saveFile(PhysicalFile $file): bool
    {
        $errors = $this->validator->validate($file);

        if(count($errors) > 0) {
            return false;
        }

        $this->em->persist($file);
        $this->em->flush();

        return true;
    }

    public function updateFile(PhysicalFile $file): bool
    {
        $errors = $this->validator->validate($file);

        if(count($errors) > 0) {
            return false;
        }

        $this->em->persist($file);
        $this->em->flush();

        return true;
    }

    public function removeFile(PhysicalFile $file)
    {
        $this->em->remove($file);
        $this->em->flush();
    }
}