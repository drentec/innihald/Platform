<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * DocumentService.php of project Innihald.
 * Created by user marian at 24.01.20.
 */

namespace App\Service\Entity;

use App\Entity\Document;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DocumentService
{
    private ValidatorInterface $validator;

    private EntityManagerInterface $em;

    /**
     * DocumentService constructor.
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $em
     */
    public function __construct(ValidatorInterface $validator, EntityManagerInterface $em)
    {
        $this->validator = $validator;
        $this->em = $em;
    }

    public function saveDocument(Document $document): bool
    {
        $errors = $this->validator->validate($document);

        if(count($errors) > 0) {
            return false;
        }

        $this->em->persist($document);
        $this->em->flush();

        return true;
    }

    public function updateDocument($document): bool
    {
        $errors = $this->validator->validate($document);

        if(count($errors) > 0) {
            return false;
        }

        $this->em->persist($document);
        $this->em->flush();

        return true;
    }

    public function removeDocument(Document $document)
    {
        $this->em->remove($document);
        $this->em->flush();
    }
}