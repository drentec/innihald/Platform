<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * AbstractContentExtractionMessageHandler.php of project Innihald.
 * Created by user marian at 15.02.20.
 */

namespace App\MessageHandler;


use App\Message\NewDocumentMessage;
use App\Service\Entity\DocumentContentService;
use App\Service\Entity\DocumentService;
use App\Service\Entity\PhysicalFileService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AbstractContentExtractionMessageHandler implements MessageHandlerInterface
{
    private DocumentService $documentService;

    private DocumentContentService $documentContentService;

    private PhysicalFileService $fileService;

    /**
     * AbstractContentExtractionMessageHandler constructor.
     * @param DocumentService $documentService
     * @param DocumentContentService $documentContentService
     * @param PhysicalFileService $fileService
     */
    public function __construct(DocumentService $documentService, DocumentContentService $documentContentService, PhysicalFileService $fileService)
    {
        $this->documentService = $documentService;
        $this->documentContentService = $documentContentService;
        $this->fileService = $fileService;
    }

    public function __invoke(NewDocumentMessage $message)
    {
        // TODO: Implement __invoke() method.
    }
}